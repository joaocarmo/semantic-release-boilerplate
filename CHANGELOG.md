# Changelog

# [3.0.0](https://gitlab.com/joaocarmo/semantic-release-boilerplate/compare/v2.0.0...v3.0.0) (2020-07-31)


### Features

* **lib/index.js:** removing the classes ([59d579b](https://gitlab.com/joaocarmo/semantic-release-boilerplate/commit/59d579b58a8df15945387ab3919512e2a74bee7b))


### BREAKING CHANGES

* **lib/index.js:** The main export is empty now

# [2.0.0](https://gitlab.com/joaocarmo/semantic-release-boilerplate/compare/v1.1.1...v2.0.0) (2020-07-31)


### Features

* **lib/index.js:** exports Person, Teacher and Student classes ([2d6861a](https://gitlab.com/joaocarmo/semantic-release-boilerplate/commit/2d6861a8a59946029b17469e4f92dd12617bb2e3))


### BREAKING CHANGES

* **lib/index.js:** No longer exporting a single function

## [1.1.1](https://gitlab.com/joaocarmo/semantic-release-boilerplate/compare/v1.1.0...v1.1.1) (2020-07-31)


### Bug Fixes

* **lib/index.js:** removed the debug output ([5b3f56a](https://gitlab.com/joaocarmo/semantic-release-boilerplate/commit/5b3f56ad4d7158b921801473eb8ebbb24b3d42c2))
