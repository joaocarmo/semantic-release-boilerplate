# semantic-release-boilerplate

[![semantic-release][srbadge]][sr]
[![Commitizen friendly][commitizenbadge]][commitizen]

A boilerplate node project using semantic-release

## Usage

Use `git` as you normally would and a prompt should guide you through the
commit message process.

```sh
git add . && git commit
```

## Authentication

### Push access to the remote repository

`semantic-release` requires push access to the project Git repository in order
to create Git tags. The Git authentication can be set with one of the following
environment variables:

| Variable                     | Description                     |
| ---------------------------- | ------------------------------- |
| `GL_TOKEN` or `GITLAB_TOKEN` | A GitLab personal access token. |
| `GIT_CREDENTIALS`            | URL encoded Git username and password in the format `<username>:<password>`. The username and password must each be individually URL encoded, not the : separating them. |

<!-- References -->

[srbadge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
[commitizenbadge]: https://img.shields.io/badge/commitizen-friendly-brightgreen.svg
[sr]: https://github.com/semantic-release/semantic-release
[commitizen]: http://commitizen.github.io/cz-cli
